output "table_id" {
  value       = "${aws_dynamodb_table.Sandbox.id}"
  description = "DynamoDB table ID"
}
