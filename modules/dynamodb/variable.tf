variable "dynamo_db_read_capacity" {
  description = "It's for dynamodb read capacity"
  default = 5
}

variable "dynamo_db_write_capacity" {
  description = "It's for dynamodb write capacity"
  default = 5
}
