provider "aws"{
  region     = "us-east-1"
  access_key = "AKIA6EMDAOIFCL2M75EE"
  secret_key = "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"
}
resource "aws_dynamodb_table" "Sandbox" {
  name           = "Sandbox"
  billing_mode   = "PROVISIONED"
  read_capacity  = "${var.dynamo_db_read_capacity}"
  write_capacity = "${var.dynamo_db_write_capacity}"
  hash_key       = "UserId"
  range_key      = "GameTitle"

  attribute {
    name = "UserId"
    type = "S"
  }

  attribute {
    name = "GameTitle"
    type = "S"
  }

  tags = {
    Name        = "dynamodb-table-1"
    Environment = "production"
  }
}

resource "aws_dynamodb_table" "Dev" {
  name           = "Dev"
  billing_mode   = "PROVISIONED"
  read_capacity  = "${var.dynamo_db_read_capacity}"
  write_capacity = "${var.dynamo_db_write_capacity}"
  hash_key       = "UserId"
  range_key      = "GameTitle"

  attribute {
    name = "UserId"
    type = "S"
  }

  attribute {
    name = "GameTitle"
    type = "S"
  }

  tags = {
    Name        = "dynamodb-table-1"
    Environment = "production"
  }
}

resource "aws_appautoscaling_target" "dynamodb_table_read_target" {
  max_capacity       = 100
  min_capacity       = 5
  resource_id        = "table/Dev"
  scalable_dimension = "dynamodb:table:ReadCapacityUnits"
  service_namespace  = "dynamodb"
}

resource "aws_appautoscaling_policy" "dynamodb_table_read_policy" {
  name               = "DynamoDBReadCapacityUtilization:${aws_appautoscaling_target.dynamodb_table_read_target.resource_id}"
  policy_type        = "TargetTrackingScaling"
  resource_id        = "aws_appautoscaling_target.dynamodb_table_read_target.resource_id"
  scalable_dimension = "aws_appautoscaling_target.dynamodb_table_read_target.scalable_dimension"
  service_namespace  = "aws_appautoscaling_target.dynamodb_table_read_target.service_namespace"

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "DynamoDBReadCapacityUtilization"
    }

    target_value = 70
  }
}

resource "aws_appautoscaling_target" "dynamodb_table_write_target" {
  max_capacity       = 100
  min_capacity       = 5
  resource_id        = "table/Dev"
  scalable_dimension = "dynamodb:table:WriteCapacityUnits"
  service_namespace  = "dynamodb"
}

resource "aws_appautoscaling_policy" "dynamodb_table_write_policy" {
  name               = "DynamoDBWriteCapacityUtilization:${aws_appautoscaling_target.dynamodb_table_write_target.resource_id}"
  policy_type        = "TargetTrackingScaling"
  resource_id        = "aws_appautoscaling_target.dynamodb_table_write_target.resource_id"
  scalable_dimension = "aws_appautoscaling_target.dynamodb_table_write_target.scalable_dimension"
  service_namespace  = "aws_appautoscaling_target.dynamodb_table_write_target.service_namespace"

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "DynamoDBWriteCapacityUtilization"
    }

    target_value = 70
  }
}
