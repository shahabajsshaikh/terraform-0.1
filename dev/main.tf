
module "dynamodb_table" {
  source = "/home/ec2-user/terraform-0.1/modules/dynamodb"
   dynamo_db_read_capacity = "${var.dynamo_dev_read_capacity}"
   dynamo_db_write_capacity = 4
}
